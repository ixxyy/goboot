package hash

import "crypto/md5"

func GenerateMd5(input string) string {
	m5 := md5.New()
	m5.Write([]byte(input))
	return ToHexString(m5.Sum(nil))
}
