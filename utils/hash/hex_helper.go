package hash

import (
	"encoding/hex"
	"fmt"
	"strconv"
)

func ToHexString(input []byte) string {
	return fmt.Sprintf("%x", input)
}

func HexToBytes(strHex string) []byte {
	hashBytes, _ := hex.DecodeString(strHex)
	return hashBytes
}

func HexToInt(strHex string) int {
	n, _ := strconv.ParseUint(strHex, 16, 32)
	return int(n)
}

func IntToHex(input int) string {
	return fmt.Sprintf("%x", input)
}
