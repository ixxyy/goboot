package file

import "os"

//
//  Exists
//  @Description:
//  @param name
//  @return bool
//
func Exists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

//
//  MkDir
//  @Description:
//  @param folderPath
//  @return error
//
func MkDir(folderPath string) error {
	// 先创建文件夹
	err := os.Mkdir(folderPath, 0777)
	if err != nil {
		return err
	}
	// 再修改权限
	err = os.Chmod(folderPath, 0777)
	if err != nil {
		return err
	}
	return nil
}
