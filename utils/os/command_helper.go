package os

import (
	"context"
	"log"
	"os/exec"
	"regexp"
	"time"
)

//CommandExecResult 命令执行结果
type CommandExecResult struct {
	ExitCode int
	Message  string
}

//ExecCommand 执行指定命令，并返回执行结果
func ExecCommand(command, args string, timeout time.Duration) CommandExecResult {
	result := CommandExecResult{-1, "执行命令失败"}
	if command != "" {
		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		defer cancel()
		argsList := make([]string, 0, 10)
		if args != "" {
			r := regexp.MustCompile("[^\\s]+")
			argsList = append(argsList, r.FindAllString(args, -1)...)
		}
		log.Println("SHELL:", args)
		cmd := exec.CommandContext(ctx, command, argsList...)
		out, err := cmd.CombinedOutput()
		exitCode := cmd.ProcessState.ExitCode()
		result.ExitCode = exitCode
		log.Println("run result:", exitCode, string(out))
		if err != nil {
			if out != nil {
				result.Message = string(out)
			} else {
				result.Message = err.Error()
			}

		} else {
			result.Message = string(out)
		}
	}
	defer func() {
		if err := recover(); err != nil {
			log.Println(err)
		}
	}()
	return result
}
