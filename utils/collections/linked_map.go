package collections

import "gitee.com/ixxyy/goboot/utils/text/gstr"

type LinkedMap struct {
	LinkedKeyArray []interface{}
	Map            map[interface{}]interface{}
}

type MapItem struct {
	Key   interface{}
	Value interface{}
}

func (m *LinkedMap) Put(key interface{}, value interface{}) {
	if m.Map == nil {
		m.Map = make(map[interface{}]interface{}, 3)
	}

	_, ok := m.Map[key]
	if !ok {
		if !Contains(key, m.LinkedKeyArray) {
			m.LinkedKeyArray = append(m.LinkedKeyArray, key)
		}
	}
	m.Map[key] = value
}

func (m *LinkedMap) Get(key interface{}) interface{} {
	if m.Map == nil {
		return nil
	}
	value, ok := m.Map[key]
	if !ok {
		return nil
	} else {
		return value
	}

}

func (m *LinkedMap) ContainsKey(key interface{}) bool {
	if m.Map == nil {
		return false
	}
	_, ok := m.Map[key]
	return ok
}

//
//  Delete
//  @Description: delete element with both keys and map
//  @receiver m
//  @param key
//
func (m *LinkedMap) Delete(key interface{}) {
	delete(m.Map, key)
	DeleteElementInSlice(m.LinkedKeyArray, key)
}

//
//  Clear
//  @Description: clear collection with both keys and map
//  @receiver m
//
func (m *LinkedMap) Clear() {
	m.LinkedKeyArray = m.LinkedKeyArray[0:0]
	m.Map = make(map[interface{}]interface{})
}

//
//  GetString
//  @Description: return value in string
//  @receiver m
//  @param key
//  @return string
//
func (m *LinkedMap) GetString(key interface{}) string {
	value := m.Get(key)
	if value == nil {
		return ""
	} else {
		return gstr.ToString(value)
	}
}

func (m *LinkedMap) Items() []MapItem {
	var result []MapItem
	for _, key := range m.LinkedKeyArray {
		value := m.Get(key)
		item := MapItem{
			Key:   key,
			Value: value,
		}
		result = append(result, item)
	}
	return result
}

func (m *LinkedMap) StringItems() []MapItem {
	var result []MapItem
	for _, key := range m.LinkedKeyArray {
		value := m.GetString(key)
		item := MapItem{
			Key:   key,
			Value: value,
		}
		result = append(result, item)
	}
	return result
}
