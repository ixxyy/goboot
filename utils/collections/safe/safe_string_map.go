package safe

import (
	"gitee.com/ixxyy/goboot/utils/collections"
	"sync"
)

type StringMap struct {
	sync.RWMutex
	Map map[string]string
}

func (m *StringMap) Get(key string) (string, bool) {
	m.RLock()
	value, ok := m.Map[key]
	m.RUnlock()
	return value, ok
}

func (m *StringMap) ContainsKey(key string) bool {
	m.RLock()
	_, ok := m.Map[key]
	m.RUnlock()
	return ok
}

func (m *StringMap) Put(key string, value string) {
	m.Lock()
	m.Map[key] = value
	m.Unlock()
}

func (m *StringMap) Delete(key string) {
	m.Lock()
	delete(m.Map, key)
	m.Unlock()
}

func (m *StringMap) Clear() {
	m.Lock()
	for k := range m.Map {
		delete(m.Map, k)
	}
	m.Unlock()
}

func (m *StringMap) ToStringMap() map[string]string {
	return m.Map
}

func (m *StringMap) ToValueArray() []string {
	return collections.StringMapToSlice(m.Map)
}
