package safe

import (
	"fmt"
	"gitee.com/ixxyy/goboot/utils/collections"
	"gitee.com/ixxyy/goboot/utils/text/gstr"
	"sync"
)

type Map struct {
	sync.RWMutex
	Map map[interface{}]interface{}
}

func (m *Map) Get(key interface{}) (interface{}, bool) {
	m.RLock()
	value, ok := m.Map[key]
	m.RUnlock()
	return value, ok
}

func (m *Map) ContainsKey(key interface{}) bool {
	m.RLock()
	_, ok := m.Map[key]
	m.RUnlock()
	return ok
}

func (m *Map) Put(key interface{}, value interface{}) {
	m.Lock()
	m.Map[key] = value
	m.Unlock()
}

func (m *Map) Delete(key string) {
	m.Lock()
	delete(m.Map, key)
	m.Unlock()
}

func (m *Map) Clear() {
	m.Lock()
	for k := range m.Map {
		delete(m.Map, k)
	}
	m.Unlock()
}

func (m *Map) ToStringMap() map[string]string {
	result := make(map[string]string, len(m.Map))
	for key, value := range m.Map {
		result[gstr.ToString(key)] = fmt.Sprintf("%s", value)
	}
	return result
}

func (m *Map) ToValueArray() []string {
	return collections.StringMapToSlice(m.ToStringMap())
}
