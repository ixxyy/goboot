package ttlcache

import (
	"fmt"
	"gitee.com/ixxyy/goboot/utils/collections"
	"sync"
	"time"
)

//  Cache is a synchronised map of Items that auto-expire once stale
//  https://github.com/microsoftarchive/ttlcache
//  @Description:
//
type Cache struct {
	Mutex       sync.RWMutex
	Ttl         time.Duration
	TtlInterval time.Duration
	Items       map[string]*Item
}

// Set is a thread-safe way to add new Items to the map
func (cache *Cache) Set(key string, data string) {
	cache.SetWithTtl(key, data, cache.Ttl)
}

// Set is a thread-safe way to add new Items to the map
func (cache *Cache) SetWithTtl(key string, data string, duration time.Duration) {
	cache.Mutex.Lock()
	item := &Item{data: data}
	item.touch(duration)
	cache.Items[key] = item
	cache.Mutex.Unlock()
}

// Set is a thread-safe way to add new Items to the map
func (cache *Cache) ContainsKey(key string) (found bool) {
	cache.Mutex.Lock()
	item, exists := cache.Items[key]
	if !exists || item.expired() {
		found = false
	} else {
		found = true
	}
	cache.Mutex.Unlock()
	return
}

// Set is a thread-safe way to add new Items to the map
func (cache *Cache) ContainsKeyAndUpdate(key string, value string) (found bool) {
	cache.Mutex.Lock()
	item, exists := cache.Items[key]
	if !exists || item.expired() {
		found = false
	} else {
		if item.data != value {
			item.data = value
		}
		if cache.TtlInterval > 0 {
			item.touch(cache.TtlInterval)
		}
		found = true
	}
	cache.Mutex.Unlock()
	return
}

// Get is a thread-safe way to lookup Items
// Every lookup, also touches the item, hence extending it's life
func (cache *Cache) Get(key string) (data string, found bool) {
	cache.Mutex.Lock()
	item, exists := cache.Items[key]
	if !exists || item.expired() {
		data = ""
		found = false
	} else {
		item.touch(cache.TtlInterval)
		data = item.data
		found = true
	}
	cache.Mutex.Unlock()
	return
}

// Count returns the number of Items in the cache
// (helpful for tracking memory leaks)
func (cache *Cache) Count() int {
	cache.Mutex.RLock()
	count := len(cache.Items)
	cache.Mutex.RUnlock()
	return count
}

func (cache *Cache) cleanup() {
	cache.Mutex.Lock()
	for key, item := range cache.Items {
		if item.expired() {
			delete(cache.Items, key)
		}
	}
	cache.Mutex.Unlock()
}

func (cache *Cache) startCleanupTimer() {
	duration := cache.Ttl
	if duration < time.Second {
		duration = time.Second
	}
	ticker := time.Tick(duration)
	go (func() {
		for {
			select {
			case <-ticker:
				cache.cleanup()
			}
		}
	})()
}

// NewCache is a helper to create instance of the Cache struct
func NewCache(duration time.Duration, interval time.Duration) *Cache {
	cache := &Cache{
		Ttl:         duration,
		TtlInterval: interval,
		Items:       map[string]*Item{},
	}
	cache.startCleanupTimer()
	return cache
}

//
//  ToStringMap
//  @Description:	convert to string map
//  @receiver cache
//  @return map[string]string
//
func (cache *Cache) ToStringMap() map[string]string {
	result := make(map[string]string, len(cache.Items))
	for key, value := range cache.Items {
		if value.expired() {
			continue
		}
		result[key] = fmt.Sprintf("%s, expired#%v", value.data, value.expires.Format("2006-01-02 15:04:05"))
	}
	return result
}

//
//  ToValueArray
//  @Description:	convert values to array
//  @receiver cache
//  @return []string
//
func (cache *Cache) ToValueArray() []string {
	return collections.StringMapToSlice(cache.ToStringMap())
}
