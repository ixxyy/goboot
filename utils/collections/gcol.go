package collections

import (
	"reflect"
	"strings"
)

//
//  MapToSlice
//  @Description: convert map value to array
//  @param input
//  @return []interface{}
//
func MapToSlice(input map[string]interface{}) []interface{} {
	if input == nil {
		return nil
	}
	var output []interface{}
	for _, v := range input {
		output = append(output, v)
	}
	return output
}

//
//  StringMapToSlice
//  @Description: convert map value to string array
//  @param input
//  @return []interface{}
//
func StringMapToSlice(input map[string]string) []string {
	if input == nil {
		return nil
	}
	var output []string
	for _, v := range input {
		output = append(output, v)
	}
	return output
}

//
//  ContainsString
//  @Description:
//  @param value string
//  @param list slice of string
//  @return bool
//
func ContainsString(value string, list []string) bool {
	for _, v := range list {
		if v == value {
			return true
		}
	}
	return false
}

//
//  ContainsInt
//  @Description:
//  @param value
//  @param list
//  @return bool
//
func ContainsInt(value int, list []int) bool {
	for _, v := range list {
		if v == value {
			return true
		}
	}
	return false
}

//
//  Contains
//  @Description: slice | Array | Map .contains interface{}
//  @param obj element
//  @param target collection such as slice | Array | Map
//  @return bool
//
func Contains(obj interface{}, target interface{}) bool {
	targetValue := reflect.ValueOf(target)
	switch reflect.TypeOf(target).Kind() {
	case reflect.Slice, reflect.Array:
		for i := 0; i < targetValue.Len(); i++ {
			if targetValue.Index(i).Interface() == obj {
				return true
			}
		}
	case reflect.Map:
		if targetValue.MapIndex(reflect.ValueOf(obj)).IsValid() {
			return true
		}
	}

	return false
}

func FindElementInSlice(a []interface{}, x interface{}) int {
	for i, n := range a {
		if x == n {
			return i
		}
	}
	return -1
}

func DeleteElementInSliceByIndex(a []interface{}, i int) {
	// Shift a[i+1:] left one index
	copy(a[i:], a[i+1:])
	// Erase last element (write zero value)
	a[len(a)-1] = ""
	// Truncate slice
	a = a[:len(a)-1]
}

func DeleteElementInSlice(a []interface{}, x interface{}) bool {
	idx := FindElementInSlice(a, x)
	if idx == -1 {
		return false
	}
	DeleteElementInSliceByIndex(a, idx)
	return true
}

//
//  StartsWith
//  @Description:
//  @param value
//  @param list
//  @return bool	startsWith
//  @return string	matched string
//
func StartsWith(value string, list []string) (bool, string) {
	for _, v := range list {
		if strings.HasPrefix(value, v) {
			return true, v
		}
	}
	return false, ""

}
