package db

import (
	"gitee.com/ixxyy/goboot/app/global/app_variables"
	_ "github.com/go-sql-driver/mysql" // mysql
	"github.com/jmoiron/sqlx"
	"log"
)

func GetDatabaseClient(name string) (db *sqlx.DB, err error) {
	dsn := app_variables.ConfigDatabaseYml.GetString("db." + name + ".dsn")
	maxIdle := app_variables.ConfigDatabaseYml.GetInt("db." + name + ".MaxIdle")
	maxOpen := app_variables.ConfigDatabaseYml.GetInt("db." + name + ".MaxOpen")
	maxLifetime := app_variables.ConfigDatabaseYml.GetDuration("db." + name + ".MaxLifetime")
	log.Print(dsn, maxIdle, maxOpen, maxLifetime)
	db, err = sqlx.Connect("mysql", dsn)
	if err != nil {
		return nil, err
	}
	db.SetMaxIdleConns(maxIdle)
	db.SetMaxOpenConns(maxOpen)
	db.SetConnMaxLifetime(maxLifetime)
	return db, nil
}
