package db

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"log"
	"strings"
)

//
//  SaveOrUpdateDynamic
//  @Description: save or update by dict, dynamic generate insert/update sql
//  @param db	connection of database
//  @param table	table name
//  @param dataMap	value map
//  @param findMap  primary key to find row existed
//  @param duplicatedUpdate	true, update where row existed; false, skip
//
func SaveOrUpdateDynamic(db *sqlx.DB, table string, dataMap map[string]interface{}, findMap map[string]interface{}, duplicatedUpdate bool) {
	if db == nil || len(table) == 0 || len(dataMap) == 0 {
		return
	}

	rowExisted := false
	whereFields, whereValues := convertFindMap(findMap)
	if len(findMap) > 0 {
		// need to findData first
		sqlFindOne := fmt.Sprintf("select count(1) from %s where %s", table, strings.Join(whereFields, " and "))
		//log.Println("saveOrUpdate, find#", sqlFindOne, whereValues)
		var findResult int
		err := db.Get(&findResult, sqlFindOne, whereValues...)
		if err != nil {
			log.Println("saveOrUpdate, find error#", err)
		}

		rowExisted = findResult > 0
		//log.Println("saveOrUpdate, find result#", findResult, rowExisted)
	}

	if !rowExisted {

		insertFields, insertPlaceholder, insertValues := convertInsertValueMap(dataMap)
		sqlInsert := fmt.Sprintf("insert into %s (%s) values (%s)",
			table, strings.Join(insertFields, ","), strings.Join(insertPlaceholder, ","))
		//log.Println("saveOrUpdate, insert#", sqlInsert, insertValues)
		_, err := db.Exec(sqlInsert, insertValues...)
		if err != nil {
			log.Println("saveOrUpdate, save error", err)
		}
	} else if duplicatedUpdate {

		updateFields, updateValues := convertUpdateValueMap(dataMap)

		sqlUpdate := fmt.Sprintf("update %s set %s where %s",
			table, strings.Join(updateFields, ","), strings.Join(whereFields, " and "))
		updateValues = append(updateValues, whereValues...)
		//log.Println("saveOrUpdate, update#", sqlUpdate, updateValues)
		_, err := db.Exec(sqlUpdate, updateValues...)
		if err != nil {
			log.Println("saveOrUpdate, update error", err)
		}
	}

}

//
//  UpdateDynamic
//  @Description: update directly
//  @param db
//  @param table
//  @param dataMap
//  @param findMap
//
func UpdateDynamic(db *sqlx.DB, table string, dataMap map[string]interface{}, findMap map[string]interface{}) {
	if db == nil || len(table) == 0 || len(dataMap) == 0 || len(findMap) == 0 {
		return
	}

	updateFields, updateValues := convertUpdateValueMap(dataMap)
	whereFields, whereValues := convertFindMap(findMap)

	sqlUpdate := fmt.Sprintf("update %s set %s where %s",
		table, strings.Join(updateFields, ","), strings.Join(whereFields, " and "))
	updateValues = append(updateValues, whereValues...)
	//log.Println("UpdateDynamic, update#", sqlUpdate, updateValues)
	_, err := db.Exec(sqlUpdate, updateValues...)
	if err != nil {
		log.Println("UpdateDynamic, update error", err)
	}
}

//
//  SaveDynamic
//  @Description: save directly
//  @param db
//  @param table
//  @param dataMap
//
func SaveDynamic(db *sqlx.DB, table string, dataMap map[string]interface{}) {
	insertFields, insertPlaceholder, insertValues := convertInsertValueMap(dataMap)
	sqlInsert := fmt.Sprintf("insert into %s (%s) values (%s)",
		table, strings.Join(insertFields, ","), strings.Join(insertPlaceholder, ","))
	//log.Println("saveOrUpdate, insert#", sqlInsert, insertValues)
	_, err := db.Exec(sqlInsert, insertValues...)
	if err != nil {
		log.Println("saveOrUpdate, save error", err)
	}
}

func convertFindMap(findMap map[string]interface{}) (whereFields []string, whereValues []interface{}) {
	if len(findMap) == 0 {
		return
	}

	for k, v := range findMap {
		whereFields = append(whereFields, fmt.Sprintf("%s=?", k))
		whereValues = append(whereValues, v)
	}
	return
}

func convertUpdateValueMap(dataMap map[string]interface{}) (updateFields []string, updateValues []interface{}) {
	if len(dataMap) == 0 {
		return
	}

	for k, v := range dataMap {
		updateFields = append(updateFields, fmt.Sprintf("%s=?", k))
		updateValues = append(updateValues, v)
	}
	return
}

func convertInsertValueMap(dataMap map[string]interface{}) (insertFields []string, insertPlaceholder []string, insertValues []interface{}) {
	if len(dataMap) == 0 {
		return
	}

	for k, v := range dataMap {
		insertFields = append(insertFields, k)
		insertPlaceholder = append(insertPlaceholder, "?")
		insertValues = append(insertValues, v)
	}
	return
}
