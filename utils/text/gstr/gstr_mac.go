// @Title
// @Description
// @Author
// @Date 11/6/20 12:05 PM
package gstr

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

var (
	MacSeparators = []string{":", "-", " ", "."}
	MacRegexp     = regexp.MustCompile(`^[a-fA-F0-9]{12}$`)
)

//
//  FormatMacAddress
//  @Description: format mac address to 12 bits
//  @param input
//  @return string
//
func FormatMacAddress(input string) string {
	if input == "" {
		return input
	}
	strMacLower := strings.ToLower(input)
	if MacRegexp.MatchString(strMacLower) {
		return strMacLower
	}

	var parts []string
	for _, sep := range MacSeparators {
		if strings.Contains(strMacLower, sep) {
			parts = SplitAndTrim(strMacLower, sep)
			//匹配到之后不再处理后面的规则
			break
		}
	}

	var macArray []string
	if len(parts) == 3 && len(parts[0]) == 4 && len(parts[1]) == 4 && len(parts[2]) == 4 { //增加判断每个段的长度，避免数组越界
		macArray = []string{parts[0][0:2], parts[0][2:4], parts[1][0:2], parts[1][2:4], parts[2][0:2], parts[2][2:4]}
	} else if len(parts) == 6 {
		macArray = []string{"", "", "", "", "", ""}
		for index, data := range parts {
			if len(data) == 1 {
				data = "0" + data
			}
			macArray[index] = data
		}
	}

	return strings.Join(macArray, "")
}

func IsValidMac(input string) bool {
	formatStr := FormatMacAddress(input)
	fmt.Println(formatStr, input)
	return MacRegexp.MatchString(formatStr)
}

func IntegerStringToMac(macInt, separator string) []string {
	if len(macInt) > 0 {
		if len(separator) == 0 {
			separator = "."
		}
		intArray := strings.Split(macInt, separator)
		// 627.196.237.186.170.163.244, H3C:S5500-HI
		var macIntArray []string
		if len(intArray) > 6 {
			begin := len(intArray) - 6
			macIntArray = intArray[begin:]
		} else {
			macIntArray = intArray
		}

		if len(macIntArray) == 6 {
			mac := make([]string, 0, 6)
			for _, ms := range macIntArray {
				i, err := strconv.Atoi(ms)
				if err != nil {
					return []string{}
				}
				macI := fmt.Sprintf("%02x", i)
				mac = append(mac, macI)
			}
			return mac
		}
	}
	return []string{}
}
