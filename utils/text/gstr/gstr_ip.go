package gstr

import (
	"fmt"
	"gitee.com/ixxyy/goboot/utils/hash"
	"math"
	"net"
	"regexp"
	"strings"
)

func IPString2Long(ip string) uint {
	b := net.ParseIP(ip).To4()
	if b == nil {
		return 0
	}

	return uint(b[3]) | uint(b[2])<<8 | uint(b[1])<<16 | uint(b[0])<<24
}

// Long2IPString 把数值转为ip字符串
func Long2IPString(i uint) string {
	if i > math.MaxUint32 {
		return ""
	}

	ip := make(net.IP, net.IPv4len)
	ip[0] = byte(i >> 24)
	ip[1] = byte(i >> 16)
	ip[2] = byte(i >> 8)
	ip[3] = byte(i)

	return ip.String()
}

//IsIpAddress 判断是否是IP地址
func IsIpAddress(ip string) bool {
	if len(ip) == 0 {
		return false
	}
	address := net.ParseIP(ip)
	return address != nil
}

var (
	ipReg = regexp.MustCompile(`^((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.){3}(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])$`)
)

func ConvertIpToHex(ip string) string {
	parts := strings.Split(ip, ".")
	if len(parts) != 4 {
		return ""
	}
	var hexArray []string
	for _, part := range parts {
		intPart := StringToInt(part)
		hex := hash.IntToHex(intPart)
		if len(hex) == 1 {
			hex = "0" + hex
		}
		hexArray = append(hexArray, hex)
	}
	return strings.Join(hexArray, "")
}

func ConvertHexToIp(hex string) string {
	if len(hex) != 8 {
		return ""
	}
	ips := []string{hex[0:2], hex[2:4], hex[4:6], hex[6:8]}
	var ipArray []string
	for _, p := range ips {
		mask := hash.HexToInt(p)
		if mask >= 0 && mask <= 255 {
			ipArray = append(ipArray, ToString(mask))
		}
	}
	if len(ipArray) != 4 {
		return ""
	}
	ip := strings.Join(ipArray, ".")
	return ip

}

func IsValidIp(input string) bool {
	return ipReg.MatchString(input)
}

func HostsOfCidr(cidr string) ([]string, error) {
	ip, ipNet, err := net.ParseCIDR(cidr)
	if err != nil {
		return nil, err
	}

	var ips []string
	for ip := ip.Mask(ipNet.Mask); ipNet.Contains(ip); inc(ip) {
		ips = append(ips, ip.String())
	}
	if ips == nil {
		return nil, fmt.Errorf("empty ip array")
	}
	result := ips[1 : len(ips)-1]
	return result, nil
}

func inc(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}
