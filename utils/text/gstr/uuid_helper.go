package gstr

import (
	uuid "github.com/satori/go.uuid"
	"strings"
)

func GenerateUuid() string {
	id := uuid.NewV4()
	uid := strings.ToLower(id.String())
	return strings.ReplaceAll(uid, "-", "")
}
