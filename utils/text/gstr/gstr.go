// @Title
// @Description
// @Author
// @Date 11/6/20 9:42 PM
package gstr

import (
	"encoding/json"
	"strconv"
	"strings"
)

// SplitAndTrim splits string <str> by a string <delimiter> to an array,
// and calls Trim to every element of this array. It ignores the elements
// which are empty after Trim.
func SplitAndTrim(str, delimiter string, characterMask ...string) []string {
	array := make([]string, 0)
	for _, v := range strings.Split(str, delimiter) {
		v = Trim(v, characterMask...)
		if v != "" {
			array = append(array, v)
		}
	}
	return array
}

func SplitWithoutTrim(str, delimiter string, characterMask ...string) []string {
	array := make([]string, 0)
	for _, v := range strings.Split(str, delimiter) {
		//v = Trim(v, characterMask...)
		if v != "" {
			array = append(array, v)
		}
	}
	return array
}

func ToString(a interface{}) string {
	switch a.(type) {
	case string:
		return a.(string)
	case int:
		return strconv.Itoa(a.(int))
	default:
		value, _ := json.Marshal(a)
		return string(value)
	}
}
