package gstr

import "strconv"

func StringToInt(input string) int {
	num, err := strconv.Atoi(input)
	if err != nil {
		return 0
	} else {
		return num
	}
}

//
//  StringToIntWithDefault
//  @Description:
//  @param input
//  @param defaultVal default value
//  @return int
//
func StringToIntWithDefault(input string, defaultVal int) int {
	num, err := strconv.Atoi(input)
	if err != nil {
		return defaultVal
	} else {
		return num
	}
}

func StringToFloat64(input string, defaultVal float64) float64 {
	floatVar, err := strconv.ParseFloat(input, 64)
	if err != nil {
		return defaultVal
	} else {
		return floatVar
	}
}
