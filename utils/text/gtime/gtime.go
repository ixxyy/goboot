package gtime

import "time"

func GetNowString() string {
	return time.Now().Format(dateTimeLayout)
}

func GetTimestampWithString() string {
	return time.Now().Format(dateTimeInMilliSecondsLayout)
}

const (
	dateLayout                   = "2006-01-02"
	dateTimeLayout               = "2006-01-02 15:04:05"
	dateTimeInMilliSecondsLayout = "2006-01-02 15:04:05.000"
	dateTimeFileName             = "20060102_150405"
)

//
//  GetTimestampInUnix
//  @Description: now in second, unix timestamp
//  @return int64
//
func GetTimestampInUnix() int64 {
	return time.Now().Unix()
}

//
//  GetNowInMilliSecond
//  @Description: now in ms
//  @return int64
//
func GetNowInMilliSecond() int64 {
	return time.Now().UnixNano() / 1e6
}

func ParseTimestampToDate(a int64) string {
	return time.Unix(a, 0).Format(dateLayout)
}

func ParseTimestampToDatetime(a int64) string {
	return time.Unix(a, 0).Format(dateTimeLayout)
}

func GetNowWithFileName() string {
	return time.Now().Format(dateTimeLayout)
}
