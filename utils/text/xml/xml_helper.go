package device

import "github.com/beevik/etree"

//
//  GetTagText
//  @Description:
//  @param element
//  @param name	tag name
//  @return string
//
func GetTagText(element *etree.Element, name string) string {
	child := element.SelectElement(name)
	if child != nil {
		return child.Text()
	} else {
		return ""
	}
}

//
//  GetXmlTextByPath
//  @Description: get element's text by path
//  @param doc	Document
//  @param path	such as /Notify/Location/Longitude
//  @return string
//
func GetXmlTextByPath(doc *etree.Document, path string) string {
	var text string
	element := doc.FindElement(path)
	if element != nil {
		//log.Println("deviceId", deviceId)
		text = element.Text()
		element = nil
	}
	return text
}
