package snmp

import "gitee.com/ixxyy/goboot/utils/text/gstr"

type Log struct {
	Command string   `json:"command"`
	Logs    []string `json:"logs"`
}

type LogWrapper struct {
	Logs map[string]*Log
}

//
//  AddCommandLog
//  @Description:
//  @receiver lw
//  @param key
//  @param command
//  @param content
//  @param err
//
func (lw *LogWrapper) AddCommandLog(key string, command string, content []string, err error) {

	target, ok := lw.Logs[key]
	if !ok { //
		target = &Log{Command: command, Logs: content}
		if err != nil {
			target.Logs = []string{err.Error()}
		}
		//log.Printf("addLog, %s, %v", command, content)
		lw.Logs[key] = target
	}
}

func (lw *LogWrapper) AddLog(key string, command string, content []string, err error) {
	lw.AddCommandLog(key, command, content, err)
}

//
//  AddSimpleLog
//  @Description: add log without command
//  @receiver lw
//  @param key	if key exists, append
//  @param content anything
//
func (lw *LogWrapper) AddSimpleLog(key string, content ...interface{}) {

	target, ok := lw.Logs[key]
	if !ok { //
		var data []string
		for _, row := range content {
			data = append(data, gstr.ToString(row))
		}
		target = &Log{Command: "", Logs: data}
		//log.Printf("addLog, %s, %v", command, content)
		lw.Logs[key] = target
	} else {
		logs := target.Logs
		for _, row := range content {
			logs = append(logs, gstr.ToString(row))
		}
		target.Logs = logs
		lw.Logs[key] = target
	}
}

//
//  AddStats
//  @Description: add command stats
//  @receiver lw	LogWrapper
//  @param key	log-group
//  @param command	log-group.command, add -stats suffix
//  @param stats stats description
//
func (lw *LogWrapper) AddStatsLog(key string, command string, stats string) {

	target, ok := lw.Logs[key]
	if !ok {
		target = &Log{Command: command + "-stats", Logs: []string{stats}}
		lw.Logs[key] = target
	}
}

func (lw *LogWrapper) AddError(key string, err string) {

	target, ok := lw.Logs[key]
	if !ok {
		target = &Log{Command: "", Logs: []string{err}}
		lw.Logs[key] = target
	} else {
		target.Logs = append(target.Logs, err)
		lw.Logs[key] = target
	}
}
