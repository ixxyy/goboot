package snmp

import (
	"fmt"
	"gitee.com/ixxyy/goboot/utils/text/gstr"
	"regexp"
	"strings"
)

const (
	sepColon = ":"
	SepEqual = " = "
)

type MibConfig struct {
	Id          int    `json:"id"`
	Name        string `json:"name"`
	Type        string `json:"type"`
	Mib         string `json:"mib"`
	KeyPrefix   string `json:"key-prefix"`
	ValuePrefix string `json:"value-prefix"`
	Attribute   string `json:"attribute"`
	Comment     string `json:"comment"`
}

type Entity interface {
	AddProperty(property string, value string)
}

type EntityMap interface {
	// find entity in map, set property
	AddProperty(key string, property string, value string)
}

// parse to map
// example in debug_service
func ParseMap(lines []string, config MibConfig, dataMap map[string]string) {
	if lines == nil || len(lines) == 0 {
		return
	}
	for _, line := range lines {
		if line == "" {
			continue
		}
		parts := gstr.SplitAndTrim(line, SepEqual)
		if len(parts) == 1 {
			continue
		}
		partFront := parts[0]
		partLast := parts[1]

		if strings.HasPrefix(partFront, config.KeyPrefix) {
			valuePattern := config.ValuePrefix + sepColon

			regValue, err := regexp.Compile(valuePattern)
			if err != nil {
				continue
			}

			value := regValue.ReplaceAllString(partLast, "")
			if value != "" {
				dataMap[config.Attribute] = value
			}
		}
	}

}

func ParseObject(lines []string, entity Entity, config MibConfig) {
	for _, line := range lines {
		if line == "" {
			continue
		}
		parts := gstr.SplitAndTrim(line, SepEqual)
		if len(parts) == 1 {
			continue
		}
		partFront := parts[0]
		partLast := parts[1]

		if strings.HasPrefix(partFront, config.KeyPrefix) {
			valuePattern := config.ValuePrefix + sepColon

			regValue, err := regexp.Compile(valuePattern)
			if err != nil {
				continue
			}

			value := regValue.ReplaceAllString(partLast, "")
			// trim
			value = gstr.Trim(value)

			if value != "" {
				entity.AddProperty(config.Attribute, value)
			}
		}
	}
}

func ParseList(lines []string, entityMap EntityMap, config MibConfig) {
	for _, line := range lines {
		if line == "" {
			continue
		}
		parts := gstr.SplitAndTrim(line, SepEqual)
		if len(parts) == 1 {
			continue
		}
		parseLineByMibConfig(parts[0], parts[1], entityMap, config)
	}
}

//
// SNMPv2-SMI::mib-2.17.7.1.2.2.1.2.1.0.17.50.44.167.133 = INTEGER: 11
// SNMPv2-SMI::mib-2.17.7.1.2.2.1.2.1.44.157.30.38.66.116 = INTEGER: 11
// @param partFront, SNMPv2-SMI::mib-2.17.7.1.2.2.1.2.1.0.17.50.44.167.133
// @param partLast, INTEGER: 11
// @param config, (Mib, KeyPrefix, ValuePrefix, Attribute), how to parse snmp line
//
func parseLineByMibConfig(partFront string, partLast string, entityMap EntityMap, config MibConfig) {
	if strings.HasPrefix(partFront, config.KeyPrefix) {
		key, _ := ParseStringByPrefixRegex(partFront, config.KeyPrefix)

		if strings.Contains(partLast, config.ValuePrefix+sepColon) {
			valueTarget := gstr.SplitAndTrim(partLast, config.ValuePrefix+sepColon)
			if len(valueTarget) > 0 {
				value := valueTarget[0]
				value = gstr.Trim(value)
				if value != "" {
					entityMap.AddProperty(key, config.Attribute, value)
				}
			}
		}
		return
	}
}

func ParseStringByPrefixRegex(input string, prefix string) (string, error) {
	if input == "" {
		return "", fmt.Errorf("ParseStringByPrefixRegex error, input is empty")
	}
	if prefix == "" {
		return input, nil
	}
	regPrefix, err := regexp.Compile(prefix)
	if err != nil {
		return input, err
	}

	target := regPrefix.ReplaceAllString(input, "")
	return target, nil
}
