package debug

import (
	"gitee.com/ixxyy/goboot/utils/text/gtime"
	"log"
)

type LogEntity map[string]string

type Log []LogEntity

func (d *Log) Add(line string) {
	if line == "" {
		return
	}
	log.Println(line)
	entry := map[string]string{
		gtime.GetTimestampWithString(): line,
	}
	*d = append(*d, entry)
}
