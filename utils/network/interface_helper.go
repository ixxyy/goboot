package network

import (
	"log"
	"net"
	"strings"
)

func FindInterfaceByIp(addr string) (*net.Interface, error) {

	netInterfaces, err := net.Interfaces()
	if err != nil {
		return nil, err
	}
	for i := 0; i < len(netInterfaces); i++ {
		if (netInterfaces[i].Flags & net.FlagUp) != 0 {
			adders, _ := netInterfaces[i].Addrs()
			for _, address := range adders {
				ipv4 := ipReg.FindString(address.String())
				if strings.HasPrefix(ipv4, addr) {
					ifi := &netInterfaces[i]
					log.Printf("found target interface[%s], %s, %s", netInterfaces[i].Name, ipv4, addr)
					return ifi, nil
				}
			}
		}
	}
	return nil, nil
}

func GetLocalIps() []string {

	address, err := net.InterfaceAddrs()
	if err != nil {
		return nil
	}
	var ips []string
	for _, value := range address {
		if inet, ok := value.(*net.IPNet); ok && !inet.IP.IsLoopback() {
			if inet.IP.To4() != nil {
				ips = append(ips, inet.IP.String())
			}
		}
	}
	return ips
}
