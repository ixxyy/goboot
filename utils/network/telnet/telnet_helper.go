package telnet

import (
	"github.com/reiver/go-telnet"
	"log"
	"strconv"
	"strings"
	"time"
)

//TelnetAccountVerify 验证Telnet账号密码是否正确
func TelnetAccountVerify(host string, port int, account, password string) (bool, error) {
	conn, err := telnet.DialTo(host + ":" + strconv.Itoa(port))
	if err != nil {
		log.Println(err.Error())
		return false, err
	}
	defer conn.Close()
	ReaderTelnet(conn, "Username", time.Duration(2*time.Second))
	SenderTelnet(conn, account)
	ReaderTelnet(conn, "Password", time.Duration(2*time.Second))
	SenderTelnet(conn, password)
	out := ReaderTelnet(conn, ">", time.Duration(2*time.Second))
	return strings.Contains(out, ">"), nil
}

func ReaderTelnet(conn *telnet.Conn, expect string, timeout time.Duration) string {
	var out string
	readChan := make(chan []byte, 1)
	defer close(readChan)
	go func() {
		defer func() {
			if r := recover(); r != nil {
				//log.Println(expect, ",recover...", r)
			}
		}()
		for {
			buff := make([]byte, 1)
			n, err := conn.Read(buff)
			if err != nil {
				return
			} else {
				readChan <- buff[:n]
			}
		}

	}()

	for {
		select {
		case buff := <-readChan:
			out += string(buff)
			if strings.Contains(out, expect) {
				goto ReadOk
			}
		case <-time.After(timeout):
			goto ReadOk
		}
	}
ReadOk:
	//log.Println(out)
	return out
}

//
//  ReaderTelnetWithExpects
//  @Description: support multi expect parameters
//  @param conn
//  @param expects
//  @param timeout
//  @return string
//
func ReaderTelnetWithExpects(conn *telnet.Conn, expects []string, timeout time.Duration) string {
	var out string
	readChan := make(chan []byte, 1)
	defer close(readChan)
	go func() {
		defer func() {
			if r := recover(); r != nil {
				//log.Println(expects, ",recover new...", r)
			}
		}()
		for {
			buff := make([]byte, 1)
			n, err := conn.Read(buff)
			if err != nil {
				return
			} else {
				readChan <- buff[:n]
			}
		}

	}()

	for {
		select {
		case buff := <-readChan:
			out += string(buff)
			for _, expect := range expects {
				if strings.Contains(out, expect) {
					goto ReadOk
				}
			}
		case <-time.After(timeout):
			goto ReadOk
		}
	}
ReadOk:
	//log.Println(out)
	return out
}

func SenderTelnet(conn *telnet.Conn, command string) {
	//var commandBuffer []byte
	//for _, char := range command {
	//	commandBuffer = append(commandBuffer, byte(char))
	//}
	//var crlfBuffer []byte = []byte{'\r', '\n'}
	commandBuffer := []byte(command)
	crlfBuffer := []byte{'\r', '\n'}
	_, err := conn.Write(commandBuffer)
	if err != nil {
		log.Println("send err", commandBuffer, err)
	}

	_, err = conn.Write(crlfBuffer)
	if err != nil {
		log.Println("send err", crlfBuffer, err)
	}
	//log.Println("sending command", command)
}
