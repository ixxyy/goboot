package packet

import (
	"bufio"
	"bytes"
	"io"
	"net/http"
	"net/url"
)

//
//  ReadHttpBody
//  @Description: read http body to string
//  @param r
//  @return string
//
func ReadHttpBodyToString(r io.ReadCloser) string {
	defer r.Close()
	buf := new(bytes.Buffer)
	buf.ReadFrom(r)
	return buf.String()
}

//
//  GetUrlQueryParams
//  @Description:	convert http url parameters to map
//  @param strUrl	http url
//  @return url.Values map format
//  @return error
//
func GetUrlQueryParams(strUrl string) (url.Values, error) {
	uri, err := url.ParseRequestURI(strUrl)
	if err != nil {
		return nil, err
	}
	urlParam := uri.RawQuery
	paramMap, err := url.ParseQuery(urlParam)
	if err != nil {
		return nil, err
	}
	return paramMap, nil

}

//
//  ReadRequestFromApplicationLayer
//  @Description:
//  @param payload
//  @return *http.Request
//  @return error
//
func ReadRequestFromApplicationLayer(payload []byte) (*http.Request, error) {
	payloadReader := bytes.NewReader(payload)
	bufferedPayloadReader := bufio.NewReader(payloadReader)
	return http.ReadRequest(bufferedPayloadReader)
}

//
//  ReadResponseFromApplicationLayer
//  @Description:
//  @param payload
//  @return *http.Response
//  @return error
//
func ReadResponseFromApplicationLayer(payload []byte) (*http.Response, error) {
	payloadReader := bytes.NewReader(payload)
	bufferedPayloadReader := bufio.NewReader(payloadReader)
	request := &http.Request{}
	return http.ReadResponse(bufferedPayloadReader, request)
}

//
//  GetHttpHeader
//  @Description:	get http header to map
//  @param header	http-header
//  @return map[string]string
//
func GetHttpHeader(header http.Header) map[string]string {
	ret := make(map[string]string)
	for key, values := range header {
		if values != nil && len(values) > 0 {
			ret[key] = values[0]
		}
	}
	return ret
}
