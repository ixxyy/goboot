package packet

import "github.com/google/gopacket/layers"

const (
	ProtocolNone    = "None"
	ProtocolHttp    = "http"
	ProtocolSip     = "sip"
	ProtocolFtp     = "ftp"
	ProtocolFtpData = "ftp-data"
	ProtocolTCP     = "TCP"
	ProtocolUDP     = "UDP"
	ProtocolARP     = "ARP"
	ProtocolDNS     = "DNS"
	ProtocolICMP    = "ICMP"
	L3Version4      = "IPV4"
	L3Version6      = "IPV6"
	LayerPayload    = "Payload"
)

var TcpPortNames = map[layers.TCPPort]string{
	20:   "ftp-data",
	21:   "ftp",
	22:   "ssh",
	23:   "telnet",
	25:   "smtp",
	49:   "tacacs",
	65:   "tacacs-ds",
	80:   "http",
	101:  "hostname",
	109:  "pop2",
	110:  "pop3",
	115:  "sftp",
	118:  "sqlserv",
	123:  "ntp",
	137:  "netbios-ns",
	138:  "netbios-dgm",
	139:  "netbios-ssn",
	143:  "imap",
	156:  "sqlsrv",
	161:  "snmp",
	162:  "snmptrap",
	220:  "imap3",
	389:  "ldap",
	443:  "https",
	445:  "microsoft-ds",
	546:  "dhcpv6-client",
	547:  "dhcpv6-server",
	5060: "sip",
	5070: "sip",
	1433: "ms-sql-s",
	1434: "ms-sql-m",
	1521: "oracle",
	3306: "mysql",
	3389: "ms-wbt-server",
	6379: "redis",
	8080: "http-alt",
}

var UdpPortNames = map[layers.UDPPort]string{
	7:    "echo",
	20:   "ftp-data",
	21:   "ftp",
	22:   "ssh",
	23:   "telnet",
	25:   "smtp",
	80:   "http",
	554:  "rtsp",
	5060: "sip",
	5061: "sips",
	5070: "sip",
	3389: "ms-wbt-server",
}
