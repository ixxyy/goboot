package packet

import (
	"gitee.com/ixxyy/goboot/utils/hash"
	"gitee.com/ixxyy/goboot/utils/text/gstr"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"strings"
	"time"
)

//
//  ParsePacketDefault
//  @Description: parse gopacket to FlowPacket
//  @param packet	gopacket
//  @return *FlowPacket	default struct, save with pcap format
//
func ParsePacketDefault(packet gopacket.Packet) *FlowPacket {

	ethLayer := packet.Layer(layers.LayerTypeEthernet)
	if ethLayer == nil {
		return nil
	}

	protocol := ProtocolNone
	eth := ethLayer.(*layers.Ethernet)
	etherType := eth.EthernetType.String()
	protocol = eth.EthernetType.String()
	srcMac := gstr.FormatMacAddress(eth.SrcMAC.String())
	dstMac := gstr.FormatMacAddress(eth.DstMAC.String())

	//if !service_vars.PcapCache.ContainsKey(srcMac, "") && !service_vars.PcapCache.ContainsKey(dstMac, "") {
	//	return
	//}

	var srcIp string
	var srcIpv6 string
	var srcPort int
	var dstIp string
	var dstIpv6 string
	var dstPort int
	l3Protocol := ProtocolNone
	l4Protocol := ProtocolNone

	layerList := packet.Layers()
	lastLayer := layerList[len(layerList)-1]
	protocol = lastLayer.LayerType().String()
	if protocol == LayerPayload {
		if len(layerList) > 2 {
			lastLayer = layerList[len(layerList)-2]
			protocol = lastLayer.LayerType().String()
		}
	}

	ip := ParseIpLayerWithGre(packet)

	if ip == nil {
		return nil
	} else {
		l3Protocol = L3Version4

		srcIp = ip.SrcIP.String()
		dstIp = ip.DstIP.String()
		icmpLayer := packet.Layer(layers.LayerTypeICMPv4)
		if icmpLayer != nil {
			protocol = ProtocolICMP
		}
	}

	//log.Printf("packet ok %v, %s > %s, %s", srcMac, srcIp, dstMac, dstIp)

	ipv6Layer := packet.Layer(layers.LayerTypeIPv6)
	if ipv6Layer != nil {
		l3Protocol = L3Version6
		ipv6 := ipv6Layer.(*layers.IPv6)
		srcIpv6 = ipv6.SrcIP.String()
		dstIpv6 = ipv6.DstIP.String()

		icmpLayer := packet.Layer(layers.LayerTypeICMPv6)
		if icmpLayer != nil {
			protocol = ProtocolICMP
		}
	}

	udpLayer := packet.Layer(layers.LayerTypeUDP)
	if udpLayer != nil {
		l4Protocol = ProtocolUDP
		udp := udpLayer.(*layers.UDP)
		if udp != nil {
			var udpProtocol string
			srcPort, dstPort, udpProtocol = ParseUdpProtocol(udp)
			if udpProtocol != "" {
				protocol = udpProtocol
			}
		}
	}

	tcpLayer := packet.Layer(layers.LayerTypeTCP)
	if tcpLayer != nil {
		l4Protocol = ProtocolTCP
		tcp := tcpLayer.(*layers.TCP)
		if tcp != nil {
			var tcpProtocol string
			srcPort, dstPort, tcpProtocol = ParseTcpProtocol(tcp)

			if tcpProtocol != "" {
				protocol = tcpProtocol
			}

		}
	}

	// useless
	// 0001-01-01 00:00:00 +0000 UTC, -6795364578871345152, 0001-01-01 00:00:00.000 240f9bb9252d 192.168.10.114 2c9d1e26427b 192.168.6.125 0 0 false 0
	truncated := 0
	if packet.Metadata().Truncated {
		truncated = 1
	}
	//hex :=
	//log.Println(packet.Metadata().Timestamp, ts, tsString, srcMac, srcIp, dstMac, dstIp, captureLength, length, truncated, interfaceIndex, hex)

	flow := &FlowPacket{
		SrcMac:             srcMac,
		SrcIp:              srcIp,
		SrcIpv6:            srcIpv6,
		SrcPort:            srcPort,
		DstMac:             dstMac,
		DstIp:              dstIp,
		DstIpv6:            dstIpv6,
		DstPort:            dstPort,
		MetaTimestamp:      packet.Metadata().Timestamp.Unix(),
		MetaCaptureLength:  packet.Metadata().CaptureLength,
		MetaLength:         packet.Metadata().Length,
		MetaInterfaceIndex: packet.Metadata().InterfaceIndex,
		MetaTruncated:      truncated,
		EtherType:          etherType,
		L3Version:          l3Protocol,
		L4Protocol:         l4Protocol,
		Protocol:           protocol,
		PacketData:         hash.ToHexString(packet.Data()),
		PacketLength:       len(packet.Data()),
		SampleTime:         time.Now().Unix(),
		SampleIp:           "",
	}

	appLayer := ParseAppLayer(packet)
	if appLayer != nil {
		flow.Protocol = appLayer.Protocol
		flow.RequestUri = appLayer.RequestUri
		flow.UserAgent = appLayer.UserAgent
		flow.HttpType = appLayer.HttpType
		flow.Command = appLayer.Command
		flow.HttpStatus = appLayer.HttpStatus
		flow.HttpHeaders = appLayer.HttpHeaders
		flow.Body = appLayer.Body
		flow.PassivePort = appLayer.PassivePort
	}

	return flow
}

//
//  ParseIpLayerWithGre
//  @Description: parse get layer, which has two ipv4Layer
//  @param packet
//  @return *layers.IPv4
//
func ParseIpLayerWithGre(packet gopacket.Packet) *layers.IPv4 {
	greLayer := packet.Layer(layers.LayerTypeGRE)
	var ip *layers.IPv4
	if greLayer == nil {
		ipv4Layer := packet.Layer(layers.LayerTypeIPv4)
		if ipv4Layer == nil {
			return nil
		}
		ip = ipv4Layer.(*layers.IPv4)
	} else {
		// compute gre layer dynamic
		layerCount := len(packet.Layers())
		greNum := 0
		for num, l := range packet.Layers() {
			if l.LayerType() == layers.LayerTypeGRE {
				greNum = num
			}
		}
		//log.Println("GRE layer", greNum, layerCount)
		if greNum > 0 && greNum+1 < layerCount {
			ipv4Layer := packet.Layers()[greNum+1]
			if ipv4Layer == nil || ipv4Layer.LayerType() != layers.LayerTypeIPv4 {
				return nil
			}
			ip = ipv4Layer.(*layers.IPv4)
		}
	}
	return ip
}

//
//  ParseUdpProtocol
//  @Description: default String() method return 80(http), and default layers.UDPPortNames has too many names
//  @param udp
//  @return srcPort int
//  @return dstPort int
//  @return protocol string
//
func ParseUdpProtocol(udp *layers.UDP) (srcPort int, dstPort int, protocol string) {
	srcPort = int(udp.SrcPort)
	dstPort = int(udp.DstPort)

	if name, ok := UdpPortNames[udp.SrcPort]; ok {
		protocol = name
	}
	if name, ok := UdpPortNames[udp.DstPort]; ok {
		protocol = name
	}
	return
}

//
//  ParseTcpProtocol
//  @Description: parse tcp layer
//  @param tcp	tcp layer
//  @return srcPort	src-port
//  @return dstPort	dst-port
//  @return protocol	default protocol
//
func ParseTcpProtocol(tcp *layers.TCP) (srcPort int, dstPort int, protocol string) {
	srcPort = int(tcp.SrcPort)
	dstPort = int(tcp.DstPort)

	if name, ok := TcpPortNames[tcp.SrcPort]; ok {
		protocol = name
	}
	if name, ok := TcpPortNames[tcp.DstPort]; ok {
		protocol = name
	}
	return
}

func ParseAppLayer(packet gopacket.Packet) *AppLayer {

	appLayer := packet.ApplicationLayer()
	if appLayer == nil {
		return nil
	}

	payloadContent := string(appLayer.Payload())
	if strings.HasPrefix(payloadContent, "GET") || strings.HasPrefix(payloadContent, "POST") {
		// request
		request, err := ReadRequestFromApplicationLayer(appLayer.Payload())
		if err == nil {
			httpLayer := &AppLayer{
				HttpType:   "request",
				Command:    request.Method,
				RequestUri: request.RequestURI,
				Protocol:   ProtocolHttp,
			}
			ret := GetHttpHeader(request.Header)
			httpLayer.HttpHeaders = ret
			httpLayer.UserAgent = ret["User-Agent"]
			httpLayer.Body = ReadHttpBodyToString(request.Body)
			return httpLayer
		} else {
			//log.Println("parse http-get error", err)
		}
	} else if strings.HasPrefix(payloadContent, "HTTP") {
		response, err := ReadResponseFromApplicationLayer(appLayer.Payload())
		if err == nil {
			httpLayer := &AppLayer{
				HttpType:   "response",
				HttpStatus: gstr.ToString(response.StatusCode),
				Protocol:   ProtocolHttp,
			}
			if response.Request != nil {
				httpLayer.RequestUri = response.Request.RequestURI
			}
			ret := GetHttpHeader(response.Header)
			httpLayer.HttpHeaders = ret
			httpLayer.Body = ReadHttpBodyToString(response.Body)
			return httpLayer
		} else {
			//log.Println("parse http-post error", err)
		}
	} else {
		isFtp, command, passivePort := isFtpCommand(payloadContent)
		if isFtp {
			ftpLayer := &AppLayer{
				Protocol: ProtocolFtp,
				Command:  command,
				Body:     payloadContent,
			}
			ftpLayer.PassivePort = passivePort
			return ftpLayer
		}
	}
	return nil
}
