package packet

import "gitee.com/ixxyy/goboot/utils/text/gstr"

//
//  FlowPacket
//  @Description: flow packet
//
type FlowPacket struct {
	Seq                uint64
	SrcMac             string `json:"SrcMac"`
	SrcIp              string `json:"SrcIp"`
	SrcIpv6            string `json:"SrcIpv6"`
	SrcPort            int    `json:"SrcPort"`
	DstMac             string `json:"DstMac"`
	DstIp              string `json:"DstIp"`
	DstIpv6            string `json:"DstIpv6"`
	DstPort            int    `json:"DstPort"`
	MetaTimestamp      int64
	MetaCaptureLength  int
	MetaLength         int
	MetaInterfaceIndex int
	MetaTruncated      int
	EtherType          string `json:"EtherType"`
	L3Version          string `json:"L3Version"`
	L4Protocol         string `json:"L4Protocol"`
	Protocol           string `json:"Protocol"`
	PacketData         string `json:"PacketData"`
	PacketLength       int    `json:"PacketLength"`
	SampleTime         int64  `json:"TimeReceived"`
	SampleIp           string `json:"SamplerIp"`

	HttpType    string            `json:"HttpType"`
	Command     string            `json:"Command"`
	HttpHeaders map[string]string `json:"HttpHeaders"`
	UserAgent   string            `json:"UserAgent"`
	HttpStatus  string            `json:"HttpStatus"`
	RequestUri  string            `json:"RequestUri"`
	Body        string            `json:"body"`
	PassivePort int               `json:"PassivePort"`
}

//
//  getServiceHost
//  @Description: get target service ip and port
//  @receiver flow
//  @return string
//
func (flow *FlowPacket) getServiceHost() string {
	if flow.DstPort <= 0 && !gstr.IsValidIp(flow.DstIp) {
		return ""
	}
	return flow.DstIp + ":" + gstr.ToString(flow.DstPort)
}
