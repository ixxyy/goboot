package packet

import (
	"gitee.com/ixxyy/goboot/utils/collections"
	"gitee.com/ixxyy/goboot/utils/text/gstr"
	"strings"
)

var (
	//    USER：指定用户名以进行身份验证。
	//    PASS：指定密码以进行身份验证。
	//    CWD（Change Working Directory）：更改当前工作目录。
	//    CDUP（Change to Parent Directory）：切换到父目录。
	//    PWD（Print Working Directory）：打印当前工作目录的路径。
	//    LIST：列出当前目录的文件和子目录。
	//    NLST（Name List）：仅列出当前目录的文件和子目录的名称。
	//    RETR（Retrieve）：从服务器下载文件。
	//    STOR（Store）：向服务器上传文件。
	//    DELE（Delete）：删除服务器上的文件。
	//    MKD（Make Directory）：创建新目录。
	//    RMD（Remove Directory）：删除目录。
	//    RNFR（Rename From）和RNTO（Rename To）：重命名文件或目录。
	//    SITE：向服务器发送特定于站点的命令。
	//    SYST（System）：返回服务器的操作系统类型。
	//    TYPE：设置数据传输的类型（例如，二进制或文本）。
	//    PASV（Passive）：在被动模式下启用数据连接。
	//    ABOR（Abort）：终止当前的数据连接。
	//	StatusInitiating    = 100
	//	StatusRestartMarker = 110
	//	StatusReadyMinute   = 120
	//	StatusAlreadyOpen   = 125
	//	StatusAboutToSend   = 150
	//
	//	StatusCommandOK             = 200
	//	StatusCommandNotImplemented = 202
	//	StatusSystem                = 211
	//	StatusDirectory             = 212
	//	StatusFile                  = 213
	//	StatusHelp                  = 214
	//	StatusName                  = 215
	//	StatusReady                 = 220
	//	StatusClosing               = 221
	//	StatusDataConnectionOpen    = 225
	//	StatusClosingDataConnection = 226
	//	StatusPassiveMode           = 227
	//	StatusLongPassiveMode       = 228
	//	StatusExtendedPassiveMode   = 229
	//	StatusLoggedIn              = 230
	//	StatusLoggedOut             = 231
	//	StatusLogoutAck             = 232
	//	StatusAuthOK                = 234
	//	StatusRequestedFileActionOK = 250
	//	StatusPathCreated           = 257
	//
	//	StatusUserOK             = 331
	//	StatusLoginNeedAccount   = 332
	//	StatusRequestFilePending = 350
	//
	//	StatusNotAvailable             = 421
	//	StatusCanNotOpenDataConnection = 425
	//	StatusTransfertAborted         = 426
	//	StatusInvalidCredentials       = 430
	//	StatusHostUnavailable          = 434
	//	StatusFileActionIgnored        = 450
	//	StatusActionAborted            = 451
	//	Status452                      = 452
	//
	//	StatusBadCommand              = 500
	//	StatusBadArguments            = 501
	//	StatusNotImplemented          = 502
	//	StatusBadSequence             = 503
	//	StatusNotImplementedParameter = 504
	//	StatusNotLoggedIn             = 530
	//	StatusStorNeedAccount         = 532
	//	StatusFileUnavailable         = 550
	//	StatusPageTypeUnknown         = 551
	//	StatusExceededStorage         = 552
	//	StatusBadFileName             = 553
	//	FTP status codes, defined in RFC 959
	//	https://github.com/jlaffaye/ftp/blob/master/status.go
	ftpCommandArray = []string{"USER", "PASS", "CWD", "CDUP", "PWD", "LIST", "NLIST", "RETR", "STOP",
		"DELE", "MKD", "RMD", "RNFR", "SITE", "SYST", "TYPE", "ABOR", "APPE", "PASV", "MODE", "REST", "QUIT",
		"MFMT", "MDTM", "AUTH", "FEAT", "OPTS", "EPSV",
		"100", "110", "120", "125", "150",
		"200", "202", "211", "212", "213", "214", "215",
		"220", "221", "225", "226", "227", "228", "229",
		"230", "231", "232", "234",
		"250", "257",
		"331", "332",
		"421", "425", "426", "430", "434", "450", "451", "452",
		"500", "501", "502", "503", "504",
		"530", "532", "550", "551", "552", "553"}

	ftpPassiveRespPrefix = "227"
)

//
//  isFtpCommand
//  @Description: parse ftp command
//  @param payloadContent
//  @return isFtp
//  @return command	ftp command
//  @return passivePort
//
func isFtpCommand(payloadContent string) (isFtp bool, command string, passivePort int) {
	isFtp, command = collections.StartsWith(payloadContent, ftpCommandArray)
	if strings.HasPrefix(payloadContent, ftpPassiveRespPrefix) {
		parts := gstr.SplitAndTrim(payloadContent[:len(payloadContent)-3], ",")
		lenParts := len(parts)
		// 将服务器的IP地址和端口号的高8位与低8位进行组合
		// "227 Entering Passive Mode (192,168,0,1,48,57)"，
		// 其中192.168.0.1是服务器的IP地址，端口号为（48 * 256 + 57）= 12345
		high := gstr.StringToInt(parts[lenParts-2])
		low := gstr.StringToInt(parts[lenParts-1])
		if high == 0 || low == 0 {
			return
		}
		passivePort = high*256 + low
		//log.Println("passivePort", high, low, passivePort)
	}
	return
}
