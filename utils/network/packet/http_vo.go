package packet

type AppLayer struct {
	Protocol    string            `json:"Protocol"`
	HttpType    string            `json:"HttpType"`
	HttpStatus  string            `json:"HttpStatus"`
	RequestUri  string            `json:"RequestUri"`
	Body        string            `json:"Body"`
	HttpHeaders map[string]string `json:"HttpHeaders"`
	UserAgent   string            `json:"UserAgent"`
	Command     string            `json:"Command"`
	PassivePort int               `json:"PassivePort"`
}
