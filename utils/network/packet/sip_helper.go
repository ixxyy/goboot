package packet

import (
	"bytes"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"io"
)

//
//  GetSipLayer
//  @Description:	get sipLayer, include udp and tcp
//  @param packet	gopacket
//  @return layers.SIP	sip layer
//
func GetSipLayer(packet gopacket.Packet) *layers.SIP {
	sipPacket := packet.Layer(layers.LayerTypeSIP)
	var sipLayer *layers.SIP
	if sipPacket == nil {
		sipLayer = handleTcpSip(packet)
	} else {
		sipLayer = sipPacket.(*layers.SIP)
	}
	return sipLayer
}

//
//  handleTcpSip
//  @Description: go-packet parse tcp sip error, parse directly
//  @param packet	gopacket
//  @return *layers.SIP
//
func handleTcpSip(packet gopacket.Packet) *layers.SIP {
	tcpLayer := packet.Layer(layers.LayerTypeTCP)
	if tcpLayer == nil {
		return nil
	}
	tcpPacket := tcpLayer.(*layers.TCP)
	payload := tcpPacket.LayerPayload()

	s := layers.NewSIP()
	err := decodeFromBytes(payload, s)
	if err != nil {
		//log.Println("parse sip error", err, string(payload))
		return nil
	} else {
		return s
	}
}

//
//  decodeFromBytes
//  @Description: code from layer.sip DecodeFromBytes(data []byte, df gopacket.DecodeFeedback)
//  @param data
//  @param s
//  @return error
//
func decodeFromBytes(data []byte, s *layers.SIP) error {
	// Init some vars for parsing follow-up
	var countLines int
	var line []byte
	var err error
	var offset int

	// Iterate on all lines of the SIP Headers
	// and stop when we reach the SDP (aka when the new line
	// is at index 0 of the remaining packet)
	buffer := bytes.NewBuffer(data)

	for {

		// Read next line
		line, err = buffer.ReadBytes(byte('\n'))
		if err != nil {
			if err == io.EOF {
				if len(bytes.Trim(line, "\r\n")) > 0 {

				}
				break
			} else {
				return err
			}
		}
		offset += len(line)

		// Trim the new line delimiters
		line = bytes.Trim(line, "\r\n")

		// Empty line, we hit Body
		if len(line) == 0 {
			break
		}

		// First line is the SIP request/response line
		// Other lines are headers
		if countLines == 0 {
			err = s.ParseFirstLine(line)
			if err != nil {
				return err
			}

		} else {
			err = s.ParseHeader(line)
			if err != nil {
				return err
			}
		}

		countLines++
	}
	s.BaseLayer = layers.BaseLayer{Contents: data[:offset], Payload: data[offset:]}

	return nil
}
