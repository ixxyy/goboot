package network

import (
	"fmt"
	"golang.org/x/net/ipv4"
	"log"
	"net"
	"strings"
	"time"
)

const TextLineSeparator = "\r\n"

func SendUdpData(host string, port int, message []byte, timeout time.Duration, bufferSize int) (string, error) {
	var result = ""

	addr := fmt.Sprintf("%s:%d", host, port)
	conn, err := net.DialTimeout("udp", addr, timeout)
	if err != nil {
		return result, err
	}
	err = conn.SetReadDeadline(time.Now().Add(timeout))
	if err != nil {
		return result, err
	}
	defer conn.Close()

	_, err = conn.Write(message)
	if err != nil {
		return result, err
	}

	buffer := make([]byte, bufferSize)
	n, err := conn.Read(buffer[0:])
	if err != nil {
		return result, err
	}
	result = string(buffer[:n])
	return result, nil
}

func SendMultiCastDataByInterface(ni *net.Interface, remoteAddr *net.UDPAddr, message string, respPrefix string, timeout time.Duration) ([]string, error) {

	conn, err := net.ListenUDP("udp4", remoteAddr)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	pc := ipv4.NewPacketConn(conn)

	if err := pc.JoinGroup(ni, remoteAddr); err != nil {
		return nil, err
	}

	if _, err := conn.WriteTo([]byte(message), remoteAddr); err != nil {
		fmt.Printf("Write failed, %v", err)
	}

	buf := make([]byte, 4096)
	err = conn.SetReadDeadline(time.Now().Add(timeout))
	if err != nil {
		return nil, err
	}

	var responseArray []string
	log.Printf("SendMultiCastData[%s], %s begin", ni.Name, message)
	counter := 0
	for {
		if n, addr, err := conn.ReadFrom(buf); err != nil {
			log.Printf("receive hik error, %v, %v", addr, err.Error())
			break
		} else {

			if n <= 0 {
				continue
			}
			counter = counter + 1
			msg := string(buf[:n])
			log.Printf("udp[%s][%d]#%s", ni.Name, counter, msg)
			if respPrefix == "" {
				responseArray = append(responseArray, msg)
			} else {
				if strings.HasPrefix(msg, respPrefix) {
					// 去除换行符
					msg = strings.Replace(msg, TextLineSeparator, "", -1)
					msg = strings.Replace(msg, "\r", "", -1)
					msg = strings.Replace(msg, "\n", "", -1)
					//log.Println(msg)
					responseArray = append(responseArray, msg)
				} else {
					log.Printf("udp#%s", msg)
				}
			}

		}
	}
	err = pc.LeaveGroup(ni, remoteAddr)

	if len(responseArray) == 0 {
		return nil, fmt.Errorf("empty multicast")
	}
	return responseArray, nil

}
