package network

import (
	"gitee.com/ixxyy/goboot/utils/hash"
	"gitee.com/ixxyy/goboot/utils/text/gstr"
	"regexp"
	"strings"
)

var (
	ipReg = regexp.MustCompile(`^((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.){3}(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])$`)
)

func ConvertIpToHex(ip string) string {
	parts := strings.Split(ip, ".")
	if len(parts) != 4 {
		return ""
	}
	var hexArray []string
	for _, part := range parts {
		intPart := gstr.StringToInt(part)
		hex := hash.IntToHex(intPart)
		if len(hex) == 1 {
			hex = "0" + hex
		}
		hexArray = append(hexArray, hex)
	}
	return strings.Join(hexArray, "")
}

func ConvertHexToIp(hex string) string {
	if len(hex) != 8 {
		return ""
	}
	ips := []string{hex[0:2], hex[2:4], hex[4:6], hex[6:8]}
	var ipArray []string
	for _, p := range ips {
		mask := hash.HexToInt(p)
		if mask >= 0 && mask <= 255 {
			ipArray = append(ipArray, gstr.ToString(mask))
		}
	}
	if len(ipArray) != 4 {
		return ""
	}
	ip := strings.Join(ipArray, ".")
	return ip

}

func IsValidIp(input string) bool {
	return ipReg.MatchString(input)
}

func IsIpInGateway(ip, min, max string) bool {
	if ip < min || ip > max {
		return false
	} else {
		return true
	}
}
