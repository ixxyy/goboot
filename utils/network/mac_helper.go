package network

import (
	"fmt"
	"gitee.com/ixxyy/goboot/utils/text/gstr"
	"net"
	"strconv"
	"strings"
)

const (
	MacFormatColon3  = "colon-3"
	MacFormatColon6  = "colon-6"
	MacFormatHyphen3 = "hyphen-3"
	MacFormatHyphen6 = "hyphen-6"
	MacFormatDot6    = "dot-6"
	MacFormatNoDelim = "no-delim"
)

func ParseMac(s string) net.HardwareAddr {
	mac, _ := net.ParseMAC(s)
	return mac
}

func IntegerStringToMac(intMacStr, separ string) []string {
	if len(intMacStr) > 0 {
		if len(separ) == 0 {
			separ = "."
		}
		intArray := strings.Split(intMacStr, separ)
		// 627.196.237.186.170.163.244, H3C:S5500-HI, add vlan before
		// TODO, prefix add function
		var macIntArray []string
		if len(intArray) > 6 {
			begin := len(intArray) - 6
			macIntArray = intArray[begin:]
		} else {
			macIntArray = intArray
		}

		if len(macIntArray) == 6 {
			mac := make([]string, 0, 6)
			for _, ms := range macIntArray {
				i, err := strconv.Atoi(ms)
				if err != nil {
					return []string{}
				}
				macI := fmt.Sprintf("%02x", i)
				mac = append(mac, macI)
			}
			return mac
		}
	}
	return []string{}
}

// colon-3，冒号，0000:0000:0000
// colon-6，冒号，00:00:00:00:00:00
// hyphen-3，中划线，0000-0000-0000
// hyphen-6，中划线，00-00-00-00-00-00
// dot-6，点分割，00.00.00.00.00.00
// no-delim，无分隔符，000000000000
func FormatMac(data string, format string) (string, error) {
	mac := gstr.FormatMacAddress(data)
	if len(mac) != 12 {
		return data, fmt.Errorf("error mac#%s", data)
	}
	result := ""

	switch format {
	case MacFormatColon3:
		result = strings.Join([]string{mac[0:4], mac[4:8], mac[8:12]}, ":")
	case MacFormatColon6:
		result = strings.Join([]string{mac[0:2], mac[2:4], mac[4:6], mac[6:8], mac[8:10], mac[10:12]}, ":")
	case MacFormatHyphen3:
		result = strings.Join([]string{mac[0:4], mac[4:8], mac[8:12]}, "-")
	case MacFormatHyphen6:
		result = strings.Join([]string{mac[0:2], mac[2:4], mac[4:6], mac[6:8], mac[8:10], mac[10:12]}, "-")
	case MacFormatDot6:
		result = strings.Join([]string{mac[0:2], mac[2:4], mac[4:6], mac[6:8], mac[8:10], mac[10:12]}, ".")
	case MacFormatNoDelim:
	default:
		result = mac
	}

	return result, nil
}
