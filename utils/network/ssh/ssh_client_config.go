package ssh

//
//  ClientConfig
//  @Description: config params of ssh client
//
type ClientConfig struct {
	KeyExchanges []string

	Ciphers []string

	MACs []string

	Timeout int64
}
