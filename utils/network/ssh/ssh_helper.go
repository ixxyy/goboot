package ssh

import (
	"bytes"
	"fmt"
	"golang.org/x/crypto/ssh"
	"log"
	"strconv"
	"time"
)

//
//  RunSSHCommandWithArgs
//  @Description: add cipher\ keyExchange\ macs args, support whjz switcher, 202310
//  @param host
//  @param port
//  @param account
//  @param password
//  @param commands
//  @param ciphers
//  @param keyExchanges
//  @param macs
//  @return string
//  @return error
//
func RunSSHCommandWithArgs(host string, port int, account, password string, commands []string, clientConfig *ClientConfig) (string, error) {
	var content string
	if len(commands) <= 0 || len(account) <= 0 || len(password) <= 0 || port <= 0 {
		return content, fmt.Errorf("parameter is empty")
	}
	config := &ssh.ClientConfig{
		User: account,
		Auth: []ssh.AuthMethod{
			ssh.Password(password),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Timeout:         3 * time.Second,
	}

	config.SetDefaults()
	if clientConfig != nil {
		ciphers := clientConfig.Ciphers
		keyExchanges := clientConfig.KeyExchanges
		macs := clientConfig.MACs
		if ciphers != nil && len(ciphers) > 0 {
			config.Ciphers = ciphers
		}
		if keyExchanges != nil && len(keyExchanges) > 0 {
			config.KeyExchanges = keyExchanges
		}
		if macs != nil && len(macs) > 0 {
			config.MACs = macs
		}
		config.Timeout = time.Duration(clientConfig.Timeout) * time.Second
	}

	cli, err := ssh.Dial("tcp", host+":"+strconv.Itoa(port), config)
	if err != nil {
		log.Println(err.Error())
		return "", err
	}
	defer cli.Close()
	// Create sesssion
	sess, err := cli.NewSession()
	if err != nil {
		return "", err
	}
	defer sess.Close()

	// StdinPipe for commands
	stdin, err := sess.StdinPipe()
	if err != nil {
		log.Println(err)
		return "", err
	}

	// Uncomment to store output in variable
	var out bytes.Buffer
	var stderr bytes.Buffer
	sess.Stdout = &out
	sess.Stderr = &stderr

	// Enable system stdout
	// Comment these if you uncomment to store in variable
	//sess.Stdout = os.Stdout
	//sess.Stderr = os.Stderr

	// Start remote shell
	err = sess.Shell()
	if err != nil {
		log.Println(fmt.Sprint(err) + ": " + stderr.String())
		return "", fmt.Errorf("err#%v, %s", err.Error(), stderr.String())
	}

	for _, cmd := range commands {
		_, err = fmt.Fprintf(stdin, "%s\n", cmd)
		if err != nil {
			log.Println(fmt.Sprint(err) + ": " + stderr.String())
			return "", fmt.Errorf("err#%v, %s", err.Error(), stderr.String())
		}
	}

	// Wait for sess to finish
	err = sess.Wait()
	if err != nil {
		log.Println(err)
	}
	content = out.String()
	return content, nil

}

func RunSSHCommand(host string, port int, account, password string, commands []string) (string, error) {
	return RunSSHCommandWithArgs(host, port, account, password, commands, nil)
}

func VerifyLoginWithArgs(host string, port int, account, password string, clientConfig *ClientConfig) (bool, error) {
	config := &ssh.ClientConfig{
		User: account,
		Auth: []ssh.AuthMethod{
			ssh.Password(password),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Timeout:         3 * time.Second,
	}
	config.SetDefaults()
	if clientConfig != nil {
		ciphers := clientConfig.Ciphers
		keyExchanges := clientConfig.KeyExchanges
		macs := clientConfig.MACs
		if ciphers != nil && len(ciphers) > 0 {
			config.Ciphers = ciphers
		}
		if keyExchanges != nil && len(keyExchanges) > 0 {
			config.KeyExchanges = keyExchanges
		}
		if macs != nil && len(macs) > 0 {
			config.MACs = macs
		}
		config.Timeout = time.Duration(clientConfig.Timeout) * time.Second
	}

	cli, err := ssh.Dial("tcp", host+":"+strconv.Itoa(port), config)
	if err != nil {
		log.Println(err.Error())
		return false, err
	}
	defer cli.Close()
	return true, nil
}

func VerifyLogin(host string, port int, account, password string) (bool, error) {
	return VerifyLoginWithArgs(host, port, account, password, nil)
}
