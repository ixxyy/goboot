package response

import (
	"gitee.com/ixxyy/goboot/app/global/app_consts"
	"gitee.com/ixxyy/goboot/app/global/app_errors"
	"github.com/gin-gonic/gin"
	"net/http"
)

func ReturnJson(Context *gin.Context, httpCode int, dataCode int, msg string, data interface{}, logs interface{}) {

	//可以根据实际情况在头部添加额外的其他信息
	Context.JSON(httpCode, gin.H{
		"code": dataCode,
		"msg":  msg,
		"data": data,
		"logs": logs,
	})
}

// 将json字符窜以标准json格式返回（例如，从redis读取json、格式的字符串，返回给浏览器json格式）
func ReturnJsonFromString(Context *gin.Context, httpCode int, jsonStr string) {
	Context.Header("Content-Type", "application/json; charset=utf-8")
	Context.String(httpCode, jsonStr)
}

// 语法糖函数封装

// 直接返回成功
func Success(c *gin.Context, msg string, data interface{}, logs interface{}) {
	ReturnJson(c, http.StatusOK, app_consts.CodeSuccess, msg, data, logs)
}

// 失败的业务逻辑
func Fail(c *gin.Context, dataCode int, msg string, data interface{}, logs interface{}) {
	ReturnJson(c, http.StatusBadRequest, dataCode, msg, data, logs)
	c.Abort()
}

//权限校验失败
func ErrorAuthFail(c *gin.Context) {
	ReturnJson(c, http.StatusUnauthorized, http.StatusUnauthorized, app_errors.ErrorsNoAuthorization, "", nil)
	//暂停执行
	c.Abort()
}

//参数校验错误
func ErrorParam(c *gin.Context, wrongParam interface{}) {
	ReturnJson(c, http.StatusBadRequest, app_consts.ValidatorParamsCheckFailCode, app_consts.ValidatorParamsCheckFailMsg, wrongParam, nil)
	c.Abort()
}

// 系统执行代码错误
func ErrorSystem(c *gin.Context, msg string, data interface{}) {
	ReturnJson(c, http.StatusInternalServerError, app_consts.ServerOccurredErrorCode, app_consts.ServerOccurredErrorMsg+msg, data, nil)
	c.Abort()
}
