package main

import (
	"gitee.com/ixxyy/goboot/app/global/app_variables"
	_ "gitee.com/ixxyy/goboot/bootstrap"
	"gitee.com/ixxyy/goboot/routers"
	"log"
)

var (
	BuildTime string
	GitBranch string
	CommitId  string
	GoVersion string
)

func main() {
	app := app_variables.AppInfo{BuildTime: BuildTime, GitBranch: GitBranch,
		CommitId: CommitId, GoVersion: GoVersion}
	router := routers.InitApiRouter(app)
	_ = router.Run(app_variables.ConfigYml.GetString("HttpServer.Api.Port"))
	log.Println(BuildTime, GoVersion, GitBranch, CommitId)
}
