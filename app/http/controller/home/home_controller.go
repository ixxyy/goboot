package home

import (
	"gitee.com/ixxyy/goboot/app/global/app_variables"
	"gitee.com/ixxyy/goboot/utils/debug"
	"gitee.com/ixxyy/goboot/utils/response"
	"gitee.com/ixxyy/goboot/utils/text/gstr"
	"gitee.com/ixxyy/goboot/utils/text/gtime"
	"github.com/gin-gonic/gin"
	"log"
)

type Home struct{}

func (a *Home) Index(context *gin.Context) {
	logs := debug.Log{}
	logs.Add("welcome")
	logs.Add("hello")
	logs.Add("world")

	// 这里随便模拟一条数据返回
	response.Success(context, "ok", gin.H{
		"welcome to go-boot_api": gtime.GetNowString(),
		"ts":                     gtime.GetTimestampInUnix(),
		"uid":                    gstr.GenerateUuid(),
	}, logs)
}

func (a *Home) Version(context *gin.Context) {
	log.Println("welcome")

	// 这里随便模拟一条数据返回
	response.Success(context, "ok", gin.H{
		"app":  app_variables.App,
		"time": gtime.GetNowString(),
	}, nil)
}
