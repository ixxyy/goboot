package destroy

import (
	"gitee.com/ixxyy/goboot/app/core/event"
	"gitee.com/ixxyy/goboot/app/global/app_consts"
	"gitee.com/ixxyy/goboot/app/global/app_variables"
	"go.uber.org/zap"
	"os"
	"os/signal"
	"syscall"
)

func init() {
	//  用于系统信号的监听
	go func() {
		c := make(chan os.Signal)
		// 监听可能的退出信号
		signal.Notify(c, os.Interrupt, os.Kill, syscall.SIGQUIT, syscall.SIGINT, syscall.SIGTERM)
		//接收信号管道中的值
		received := <-c
		app_variables.ZapLog.Warn(app_consts.ProcessKilled, zap.String("信号值", received.String()))
		eventManage := event.CreateEventManageFactory()
		eventManage.FuzzyCall(app_variables.EventDestroyPrefix)
		close(c)
		os.Exit(1)
	}()

}
