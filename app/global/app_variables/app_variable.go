package app_variables

import (
	"gitee.com/ixxyy/goboot/app/global/app_errors"
	"gitee.com/ixxyy/goboot/utils/yml_config/interf"
	"go.uber.org/zap"
	"log"
	"os"
	"strings"
)

var (
	BasePath           string       // 定义项目的根目录
	Profile            string       // 应用环境, local | dev | prod
	EventDestroyPrefix = "Destroy_" //  程序退出时需要销毁的事件前缀
	ConfigKeyPrefix    = "Config_"  //  配置文件键值缓存时，键的前缀

	ZapLog            *zap.Logger            // 全局日志指针
	ConfigYml         interf.YmlConfigEntity // 全局配置文件指针
	ConfigDatabaseYml interf.YmlConfigEntity // 数据库配置文件指针

	App AppInfo
)

func init() {
	// 1.初始化程序根目录
	if path, err := os.Getwd(); err == nil {
		// 路径进行处理，兼容单元测试程序程序启动时的奇怪路径
		if len(os.Args) > 1 && strings.HasPrefix(os.Args[1], "-test") {
			BasePath = strings.Replace(strings.Replace(path, `\test`, "", 1), `/test`, "", 1)
		} else {
			BasePath = path
		}
	} else {
		log.Fatal(app_errors.ErrorsBasePath)
	}
}
