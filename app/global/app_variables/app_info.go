package app_variables

type AppInfo struct {
	BuildTime string
	GitBranch string
	CommitId  string
	GoVersion string
}
