package app_consts

const (
	// 进程被结束
	ProcessKilled string = "收到信号，进程被结束"

	// 表单验证器前缀
	ValidatorParamsCheckFailCode int    = -400300
	ValidatorParamsCheckFailMsg  string = "参数校验失败"

	//服务器代码发生错误
	ServerOccurredErrorCode int    = 5000
	ServerOccurredErrorMsg  string = "服务器内部发生代码执行错误, "

	CodeSuccess int = 0

	CurdStatusOkCode int    = 0
	CurdStatusOkMsg  string = "Success"
)
