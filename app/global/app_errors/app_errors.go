package app_errors

const (
	//系统部分
	ErrorsContainerKeyAlreadyExists string = "该键已经注册在容器中了"
	ErrorsConfigYamlNotExists       string = "config.yml 配置文件不存在"
	ErrorsStorageLogsNotExists      string = "storage/logs 目录不存在"
	ErrorsConfigDbNotExists         string = "database.yml 配置文件不存在"
	ErrorsConfigInitFail            string = "初始化配置文件发生错误"
	ErrorsFuncEventNotRegister      string = "没有找到键名对应的函数"
	ErrorsFuncEventAlreadyExists    string = "注册函数类事件失败，键名已经被注册"
	ErrorsFuncEventNotCall          string = "注册的函数无法正确执行"
	ErrorsBasePath                  string = "初始化项目根目录失败"
	ErrorsNoAuthorization           string = "token鉴权未通过，请通过token授权接口重新获取token,"

	// 数据库部分
	ErrorsDbDriverNotExists        string = "数据库驱动类型不存在,目前支持的数据库类型：mysql，您提交数据库类型："
	ErrorsDbInitFail               string = "数据库驱动初始化失败"
	ErrorsDialectorDbInitFail      string = "gorm 初始化失败,dbType:"
	ErrorsDbSqlWriteReadInitFail   string = "数据库读写分离支持的单次：Write、Read，您提交的读写分离单词："
	ErrorsDbGetConnFail            string = "从数据库连接池获取一个连接失败，超过最大连接重试次数."
	ErrorsDbPrepareRunFail         string = "sql语句预处理（prepare）失败"
	ErrorsDbQueryRunFail           string = "查询类sql语句执行失败"
	ErrorsDbExecuteRunFail         string = "执行类sql语句执行失败"
	ErrorsDbQueryRowRunFail        string = "单行查询类sql语句执行失败"
	ErrorsDbExecuteForMultipleFail string = "批量执行的sql语句执行失败"
	ErrorsDbGetEffectiveRowsFail   string = "获取sql结果影响函数失败"
	ErrorsDbTransactionBeginFail   string = "sql事务开启（begin）失败"
)
