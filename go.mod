module gitee.com/ixxyy/goboot

go 1.15

require (
	github.com/beevik/etree v1.1.0
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gin-contrib/pprof v1.3.0
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/google/gopacket v1.1.19
	github.com/jmoiron/sqlx v1.2.0
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/reiver/go-oi v1.0.0 // indirect
	github.com/reiver/go-telnet v0.0.0-20180421082511-9ff0b2ab096e
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.8.3 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
)
