package routers

import (
	_ "gitee.com/ixxyy/goboot/app/core/destroy"
	"gitee.com/ixxyy/goboot/app/global/app_variables"
	"gitee.com/ixxyy/goboot/app/http/controller/home"
	"gitee.com/ixxyy/goboot/app/http/middleware/cors"
	"gitee.com/ixxyy/goboot/utils/text/gstr"
	"github.com/gin-contrib/pprof"
	"github.com/gin-gonic/gin"
	"io"
	"log"
	"os"
)

func InitApiRouter(app app_variables.AppInfo) *gin.Engine {
	var router *gin.Engine
	app_variables.App = app
	// 非调试模式（生产模式） 日志写到日志文件
	if app_variables.ConfigYml.GetBool("AppDebug") == false {
		//1.将日志写入日志文件
		gin.DisableConsoleColor()
		f, _ := os.Create(app_variables.BasePath + app_variables.ConfigYml.GetString("Logs.GinLogName"))
		gin.DefaultWriter = io.MultiWriter(f)

		// 2.如果是有nginx前置做代理，基本不需要gin框架记录访问日志，开启下面一行代码，屏蔽上面的三行代码，性能提升 5%
		//gin.SetMode(gin.ReleaseMode)

		router = gin.Default()
	} else {
		// 调试模式，开启 pprof 包，便于开发阶段分析程序性能
		router = gin.Default()
		pprof.Register(router)
	}

	//根据配置进行设置跨域
	if app_variables.ConfigYml.GetBool("HttpServer.AllowCrossDomain") {
		router.Use(cors.Next())
	}

	log.Println("main args", gstr.ToString(app))

	apiHome := home.Home{}
	router.GET("/", apiHome.Index)

	//  创建一个门户类接口路由组
	vApi := router.Group("/api/")
	{
		vApi.GET("/", apiHome.Version)
	}
	return router
}
