package utils

import (
	"gitee.com/ixxyy/goboot/utils/text/gtime"
	"testing"
	"time"
)

func TestTime(t *testing.T) {
	t1 := gtime.GetNowInMilliSecond()
	t.Log(gtime.GetNowString())
	time.Sleep(5 * time.Second)
	t2 := gtime.GetNowInMilliSecond()
	t.Log(t2 - t1)
}
