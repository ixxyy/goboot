package utils

import (
	"gitee.com/ixxyy/goboot/utils/collections"
	"gitee.com/ixxyy/goboot/utils/collections/ttlcache"
	"testing"
	"time"
)

func TestSliceOps(t *testing.T) {
	a := []interface{}{"a", "b", "d", "c", "e"}
	i := collections.FindElementInSlice(a, "e")
	t.Log("find#", i)
	collections.DeleteElementInSlice(a, i)
	t.Log(a)
}

func TestLinkedMap(t *testing.T) {
	dict := &collections.LinkedMap{}
	dict.Put("a", 1)
	dict.Put("b", 2)
	dict.Put("z", 4)
	dict.Put("c", 3)
	dict.Put("z", 26)
	t.Log("init", dict)

	for _, item := range dict.StringItems() {
		t.Log(item.Key, item.Value)
	}

	for _, item := range dict.StringItems() {
		t.Log(item.Key, item.Value)
	}

	dict.Delete("b")
	t.Log("delete", dict)

	dict.Clear()
	t.Log("clear", dict)
}

func TestTtlCache(t *testing.T) {
	cache := ttlcache.NewCache(20*time.Second, 0*time.Second)
	data, exists := cache.Get("hello")
	if exists || data != "" {
		t.Errorf("Expected empty cache to return no data")
	}

	cache.Set("hello", "world")
	data, exists = cache.Get("hello")
	if !exists {
		t.Errorf("Expected cache to return data for `hello`")
	}
	if data != "world" {
		t.Errorf("Expected cache to return `world` for `hello`")
	}
}

func TestExpiration(t *testing.T) {
	cache := ttlcache.NewCache(10*time.Second, 0*time.Second)

	cache.Set("x", "1")
	cache.Set("y", "z")
	cache.SetWithTtl("z", "3", 20*time.Second)

	t.Log(cache.ToStringMap())
	count := cache.Count()
	if count != 3 {
		t.Errorf("Expected cache to contain 3 items")
	}

	<-time.After(500 * time.Millisecond)
	<-time.After(10 * time.Second)
	t.Log(cache.ToStringMap())
	count = cache.Count()
	if count != 1 {
		t.Errorf("Expected cache to be 1")
	}

	<-time.After(15 * time.Second)
	t.Log(cache.ToStringMap())
	count = cache.Count()
	if count != 0 {
		t.Errorf("Expected cache to be empty")
	}
}
