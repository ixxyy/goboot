package utils

import (
	"gitee.com/ixxyy/goboot/utils/text/gstr"
	"log"
	"strings"
	"testing"
)

func TestMac(t *testing.T) {
	mac := gstr.IntegerStringToMac("0.12.41.148.5.183", ".")
	t.Log(strings.Join(mac, " "))

	macs := []string{"0:c:29:4:3c:82", "00:0c:29:04:3c:82", "00:0C:29:04:3C:82", "000c-2904-3c82", "00 0c 29 04 3c 82"}
	target := "000c29043c82"
	for _, v := range macs {
		f := gstr.FormatMacAddress(v)
		log.Println(v, f, f == target, gstr.IsValidMac(v), gstr.IsValidMac(target))
	}
}
