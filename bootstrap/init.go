package bootstrap

import (
	"fmt"
	_ "gitee.com/ixxyy/goboot/app/core/destroy"
	"gitee.com/ixxyy/goboot/app/global/app_errors"
	"gitee.com/ixxyy/goboot/app/global/app_variables"
	"gitee.com/ixxyy/goboot/app/service/sys_log_hook"
	"gitee.com/ixxyy/goboot/utils/yml_config"
	"gitee.com/ixxyy/goboot/utils/zap_factory"
	"log"
	"os"
)

// 检查项目必须的非编译目录是否存在，避免编译后调用的时候缺失相关目录
func checkRequiredFolders() {
	//1.检查配置文件是否存在
	configFile := "config.yml"
	//databaseFile := "database.yml"
	if app_variables.Profile != "" {
		configFile = fmt.Sprintf("config-%s.yml", app_variables.Profile)
		//databaseFile = fmt.Sprintf("database-%s.yml", app_variables.Profile)
	}

	if _, err := os.Stat(app_variables.BasePath + "/config/" + configFile); err != nil {
		log.Fatal(app_errors.ErrorsConfigYamlNotExists + err.Error())
	}

	//if _, err := os.Stat(app_variables.BasePath + "/config/" + databaseFile); err != nil {
	//	log.Fatal(app_errors.ErrorsConfigDbNotExists + err.Error())
	//}
	//3.检查Storage/logs 目录是否存在
	if _, err := os.Stat(app_variables.BasePath + "/storage/logs/"); err != nil {
		log.Fatal(app_errors.ErrorsStorageLogsNotExists + err.Error())
	}
}

func init() {
	// 1.初始化 项目根路径，参见 variable 常量包，相关路径：app\global\variable\variable.go

	// 2.检查配置文件以及日志目录等非编译性的必要条件
	checkRequiredFolders()

	configFile := "config"
	//databaseFile := "database"
	if app_variables.Profile != "" {
		configFile = fmt.Sprintf("config-%s", app_variables.Profile)
		//databaseFile = fmt.Sprintf("database-%s", app_variables.Profile)
	}

	// 启动针对配置文件config.yml 变化的监听， 配置文件操作指针，初始化为全局变量
	app_variables.ConfigYml = yml_config.CreateYamlFactory(configFile)
	app_variables.ConfigYml.ConfigFileChangeListen()

	// database.yml 启动文件变化监听事件
	//app_variables.ConfigDatabaseYml = app_variables.ConfigYml.Clone(databaseFile)
	//app_variables.ConfigDatabaseYml.ConfigFileChangeListen()

	// 5.初始化全局日志句柄，并载入日志钩子处理函数
	app_variables.ZapLog = zap_factory.CreateZapFactory(sys_log_hook.ZapLogHandler)

	// 根据配置初始化 mysql
	//if _, err := db.GetDatabaseClient("scan"); err != nil {
	//	log.Fatal(app_errors.ErrorsDbInitFail + err.Error())
	//} else {
	//	//app_variables.DbScan = dbMysql
	//}

}
